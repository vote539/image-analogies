<!DOCTYPE html>

<title>Image Analogies</title>
<link rel="stylesheet" href="//cdn.jsdelivr.net/foundation/5.5.1/css/foundation.css" />
<style type="text/css">
body{
	padding: 10pt 30pt;
}
.ww{
	width: 320px;
	height: 240px;
}
</style>

<h1>Image Analogies: Final Project Writeup <small>by Shane Carr</small></h1>

<h2>Introduction</h2>

<p>
	<em>Man is to men as goose is to geese. Fish is to water as bird is to air. Word is to sentence as page is to book.</em>  These are all examples of <em>analogies</em>.  But what can we say about analogies involving images instead of words?
</p>

<p>
	This project implements the methods first suggested by <a href="http://mrl.nyu.edu/publications/image-analogies/analogies-72dpi.pdf">Hertzmann et al</a>.  It makes a few different assumptions, which are described in the forthcoming sections.  The code is implemented exclusively in JavaScript, which trades off performance for ease of distribution.  <a href="https://bitbucket.org/vote539/image-analogies/src">The source code is available on BitBucket</a>.  The interesting files are those housed underneath the /js directory, with the heart of the algorithm being in /js/create.js.  Note that these files are <em>not</em> a very good example of good style.  Future work should involve cleaning up and modularizing the code better.
</p>

<h2>Problem Definition</h2>

<p>
	Consider the following flower, and a blurred version of the flower.  (This is the same flower used by Hertzmann et al).
</p>

<p>
	<img src="images/newflower-src.jpg" class="ww" />
	<img src="images/newflower-blur.jpg" class="ww" />
</p>

<p>
	What can we say about the relationship between the two images?  There is some transformation of the pixels from the left image into the pixels in the right image.  But can we somehow extract this relationship and apply it to a different image, say, an image of a shoreline?
</p>

<p>
	<img src="images/toy-newshore-src.jpg" class="ww" />
</p>

<p>
	The input to the algorithm are three images: a sample input image (A), a sample output image (A'), and the source input image (B).  The output of the algorithm is an output image (B') having the same transformation that created A' from A.
</p>

<p>
	<img src="cases/shore-sc-r100-c01.png" class="ww" />
</p>

<p>
	The remaining sections describe the process of creating B' from A, A', and B.
</p>

<h2>The Algorithm</h2>

<p>
	Hertzmann suggests the following algorithm for computing an image analogy.
</p>

<p>
	<img src="images/Hertzmann-alg1.png" class="ww" />
	<img src="images/Hertzmann-alg2.png" class="ww" />
</p>

<p>
	The <em>F</em> functions compute a metric involving the specified pixel <em>p</em>.  Hertzmann suggests a Gaussian kernel over the 5x5 square centered on <em>p</em> unioned with the 3x3 square centered on <em>p</em> in the next-smaller image.  <em>BestApproximateMatch</em> implements an Approximate Nearest Neighbor search across the feature space, and <em>BestCoherenceMatch</em> helps ensure image coherence (described in a few sections).
</p>

<p>
	Having set up my code to implement Hertzmann's solution, I found that the performance in JavaScript was not acceptable.  One pixel in the output image was taking 5 seconds or more to compute.  I therefore have implemented a simpler algorithm that runs several orders of magnitude faster and produces satisfactory results.  The pseudo-code is shown below.
</p>

<pre>
function createImageAnalogy(A,A',B,B')
	s = []
	for each pixel <em>q</em> in B', in scan-line order
		p &#8592; bestMatch(A,A',B,B',s,l,q)
		B'[q] &#8592; A'[p]
		s[q] &#8592; p

function bestMatch(A,A',B,B',s,l,q)
	papp &#8592; bestApproximateMatch(A,B,q)
	pcoh &#8592; bestCoherenceMatch(A',B',s,q)

	dapp &#8592; FeatureDistance(A[papp], B[q])
	dcoh &#8592; FeatureDistance(A[pcoh], B[q])

	return pcoh if dcoh &lt; dapp*(1+COHERENCE_WEIGHT)
	else return papp

function bestApproximateMatch(A,B,q)
	for i&#8592;0; i&lt;N; i+=Math.random()*RAND_RESOLUTION
		d &#8592; FeatureDistance(A[i], B[q])
		if d &lt; best
			best &#8592; d
			p &#8592; i
	return p

function bestCoherenceMatch(Ap,Bp,s,q)
	for each of the 12 pixels <em>k</em> coming before <em>q</em>
		dr &#8592; q-k
		if s[k]+dr exists
			d &#8592; FeatureDistance(A[s[k]+dr], B[q])
			if d &lt; best
				best &#8592; d
				p &#8592; s[k]+dr
	return p
		
</pre>

<p>
	The subroutine <em>FeatureDistance</em> considers, like Hertzmann's, a 5x5 cell around the desired pixels with Gaussian weighting and normalized.  However, a major difference in my implementation is that I consider only the current image size, without considering the next image on the "pyramid."  This significantly reduces computation time, because only the final image resolution needs to be computed, rather than each of the smaller image resolutions.
</p>

<p>
	The second significant change is in my implementation of <em>bestApproximateMatch</em>.  I opted to use a randomized nearest-neighbor search, which reduces the search time by an arbitrary constant factor at the expense of more noisy output.  In practice, this assumption works very well for certain use cases, as described in more detail in the "Aerial View Synthesis" section.  Note that my search starts from the top-left of the image and continues in scan-line order.  This means that pixels closer to the top of the image are more likely to be chosen than pixels near the bottom.  Future work should involve refining this nearest neighbor search algorithm.
</p>

<p>
	Third, it is important to consider my choice of "feature vector."  In order to reduce the computation time as much as possible, I opted to use simply one number per pixel, the <em>luminance</em>, defined as the Y parameter from the YIQ color space.  Hertzmann suggests this parameter.  Color information is lost in this feature space transformation, requiring care when designing sample input and output images.
</p>

<p>
	A fourth point worth noting is how, in <em>createImageAnalogy</em>, I manage to perform the operation <code>B'[q] &#8592; A'[p]</code>.  My algorithm gives the user two choices: copy over only the luminance of A' (from the YIQ color space), or copy over the entire color definition.  In the former case, the I and Q parameters are copied from B instead of A'.  A comparison of switching this option on and off is shown in the following section.
</p>

<p>
	The code is implemented in JavaScript, enabling it to be run in a web browser.  The code is available on BitBucket (link is in the Introduction section).  Most of my sample cases were run with a randomness parameter of 100 and an image size of 160x120.
</p>

<h2>Results and Discussion</h2>

<h3>Color Parameter</h3>

<p>
	What follows is the first working image generated by my JavaScript code.
</p>

<p>
	<img src="cases/shore-fc-r100.png" class="ww" />
</p>

<p>
	It looks kind-of like a shoreline seen through a pair of pinkish glasses, almost as if the colors of the shoreline had been replaced with the colors of roses!
</p>

<p>
	Indeed, this is exactly what happened here.  All three of the Y, I, and Q parameters are copied from A' (the blurred roses picture).  As a proof of concept, I photoshopped the blurred roses picture to change the yellow flower to be blue instead.
</p>

<p>
	<img src="images/newflower-blur-blueflower.jpg" class="ww" />
</p>

<p>
	As expected, instead of being yellow, the sky is now blue, like the blue flower.
</p>

<p>
	<img src="cases/shore-bc-r100.png" class="ww" />
</p>

<p>
	This image is still not a very good analog of the original shoreline picture, though.  If we instead ask the algorithm to pull the I and Q parameters pixel-by-pixel from B, we get a very nice result.
</p>

<p>
	<img src="cases/shore-sc-r100.png" class="ww" />
</p>

<p>
	The above image was generated using a randomness parameter of 100, with coherence disabled.
</p>

<h3>Randomness Parameter</h3>

<p>
	What happens when you try to change the randomness parameters?  Below is a series of 6 images, with randomness parameters of 25, 50, 100, 200, 400, and 800, respectively.
</p>

<p>
	<img src="cases/shore-sc-r25.png" class="ww" />
	<img src="cases/shore-sc-r50.png" class="ww" />
	<img src="cases/shore-sc-r100.png" class="ww" />
	<img src="cases/shore-sc-r200.png" class="ww" />
	<img src="cases/shore-sc-r400.png" class="ww" />
	<img src="cases/shore-sc-r800.png" class="ww" />
</p>

<p>
	We can observe a very nice trend of increasing noise going from the top left to the bottom right.  This parameter can be specified by the user based on their individual needs, with the tradeoff being that lower randomness parameters lead to slower execution times.
</p>

<h3>Testing Other Assumptions</h3>

<p>
	The 5x5 square has a Gaussian kernel applied to it, giving the central pixel more weight than the neighboring pixels.  What kind of effect does this have?
</p>

<p>
	<img src="cases/shore-sc-r100.png" class="ww" />
	<img src="cases/shore-sc-r100-notg.png" class="ww" />
</p>

<p>
	The image on the left is the same poster output image as from the previous sections, with randomness parameter 100.  The image on the right is the same image, but using no Gaussian weighting (as in a "box blur").  The edges are not as crisp in the right image, consistent with the less precise nearest-neighbor lookup.
</p>

<p>
	In addition, the weight vectors are normalized by their total Gaussian weight.  This prevents pixels near the border of the image (and with less pixel data) from having lower distances than pixels near the center of the image.  For example, a corner pixel has only 8 real neighbors in the 5x5 box, and its distance may be computer to be less than a better pixel that has all 24 real neighbors.  Here is the result of turning normalization on and off.
</p>

<p>
	<img src="cases/shore-sc-r100.png" class="ww" />
	<img src="cases/shore-sc-r100-notn.png" class="ww" />
</p>

<p>
	Our favorite reference image is shown to the left, and the non-normalized image to the right.  We can clearly see that there are more artifacts in the right image, in particular little black "spots" that appear throughout the image.
</p>

<h3>The Coherence Parameter</h3>

<p>
	In the case of a blurry image where the color information is copied directly from a source image, <em>image coherence</em> is not a very large concern.  But it has a very significant effect for use cases where the color information is copied from A' instead of B.
</p>

<p>
	For example, consider the following A, A', and B images.
</p>

<p>
	<img src="images/oxbow-mask.jpg" class="ww" />
	<img src="images/oxbow.jpg" class="ww" />
	<img src="images/oxbow-newmask.jpg" class="ww" />
</p>

<p>Without a coherence parameter, we get a result image that looks something like,</p>

<p>
	<img src="cases/oxbow-r100.png" class="ww" />
</p>

<p>
	In this case, the colors are copied from the image A', as opposed to B as in the shoreline example.  We can see that the overall region definitions are correct, but all we see is a bunch of noise!
</p>

<p>
	To solve this issue, we add a <em>coherence</em> parameter, suggested by Hertzmann.  The idea is that you look at the 12 pixels <em>r</em> in the 5x5 neighborhood that come prior to the currently evaluating pixel <em>q</em> in B', go find the corresponding pixel <em>p</em> in A', and then look ahead to the pixel whose location corresponds to <em>q</em>'s location relative to <em>r</em>.  The pixel with lowest FeatureDistance weighting is then put up against the overall best pixel found by the randomized nearest neighbor search, with a bonus weight that may be specified by the user.  The algorithm then may choose to display either the nearest neighbor, or the coherence pixel.
</p>

<p>
	Below, a series of images of the shoreline are shown with varying coherence parameters.
</p>

<p>
	<img src="cases/shore-sc-r100-c01.png" class="ww" />
	<img src="cases/shore-sc-r100-c03.png" class="ww" />
	<img src="cases/shore-sc-r100-c1.png" class="ww" />
	<img src="cases/shore-sc-r100-c3.png" class="ww" />
	<img src="cases/shore-sc-r100-c10.png" class="ww" />
	<img src="cases/shore-sc-r100-c30.png" class="ww" />
	<img src="cases/shore-sc-r100-c100.png" class="ww" />
</p>

<p>
	The following graph shows the number of pixels that were captured from the randomized nearest neighbor (RNN) search versus the coherence search.  (Don't judge me by the use of Excel for making the graph.)
</p>

<p>
	<img src="images/coherence.png" />
</p>

<p style="clear:both">
	In the case of the shoreline image, an excessive coherence parameter results in a poorer quality image.  Where coherence really shines is with texture synthesis.  I encourange you to play around with different values of the coherence parameter on the live demo page.
</p>

<h2>Future Work</h2>

<p>
	The next step I would like to do would be to implement a full apprimate nearest neighbor (ANN) search and compare it to my RNN search.
</p>

<p>
	Like Hertzmann, I would also be interested in investigating more robust feature spaces.  In my JavaScript application, runtime is of course my limiting factor, so any chosen features would need to not be computationally intensive.
</p>

<p>
	Third, the idea of constructing a pyramid of images intrigues me.  It might be worth implementing this in MATLAB and comparing the results with the single-size implementation I have done here.
</p>

<h2>Other Input/Output Images: Aerial View Synthesis</h2>

<p>
	Below are the images I use for the St. Louis demo on the front page, along with a sample result.  I captured the satelite image from Google Earth.  I modified the Mississippi River to make it more blue than it is in real life.  The two masks are custom-made in Photoshop.
</p>

<p>
	<img src="images/stl-mask.jpg" class="ww" />
	<img src="images/stl-aerial.jpg" class="ww" />
	<img src="images/stl-newmask.jpg" class="ww" />
	<img src="cases/stl-r100-c200-large.png" class="ww" />
</p>

<p>
	The horizontal lines in the river in the sample output image suggest that the coherence parameter is too high.  It is telltale that the horizontal lines happen to be in scan-line order, and coherence only considers pixels that come prior in the scan line.
</p>

<p>
	Below is an aerial image of the Potomac.  They come from the Hertzmann paper.  The result is my result, not Hertzmann's result.
</p>

<p>
	<img src="images/potomac-mask.jpg" class="ww" />
	<img src="images/potomac.jpg" class="ww" />
	<img src="images/potomac-newmask.jpg" class="ww" />
	<img src="cases/potomac-r100-c1000-large.png" class="ww" />
</p>

<p>
	I modified the mask by hand to ensure that the different areas (the river, the greenery, and the city center) all had distinct luminance values.  The original masks were color-coded, but the colors had overlapping luminance.
</p>

<p>
	One thing worth noting is that the randomness parameter has an unusual effect on this use case.  In particular, a randomness parameter that is too small results in the same two or three regions of the image being replicated over and over again!  It is actually advantageous to have a larger randomness parameter in order to sample from many different places in the image.  I encourage tweaking the parameters on the live demo page to see the results.
</p>
