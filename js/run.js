var RENDER_SIZE = {width:320,height:240};
var DISPLAY_SIZE = {width:640,height:480};

function spawnWorker(RGBAs, size, takeColor, coh, res, cb){
	var worker = new Worker("js/create.js");
	worker.addEventListener("message", function(e){
		cb(e.data.result, e.data.napp, e.data.ncoh);
	});
	worker.postMessage({
		RGBAs: RGBAs,
		size: size,
		takeColor: takeColor,
		coherence: coh,
		resolution: res
	});
}

function runAlgorithm(scene, takeColor, coh, res, size){
	if(!size) size=RENDER_SIZE;
	var RGBAs = {};
	var loadCb = function(RGBAarr, im){
		RGBAs[im] = RGBAarr;

		if(typeof(RGBAs["A"])!=="undefined" &&
			typeof(RGBAs["Ap"])!=="undefined" &&
			typeof(RGBAs["B"])!=="undefined" &&
			typeof(RGBAs["Bp"])!=="undefined" ){

			console.log("Images loaded. Starting computation.");

			spawnWorker(RGBAs, size, takeColor, coh, res,
				function(result, napp, ncoh){

				var imageData = new ImageData(new Uint8ClampedArray(result), size.width, size.height);
				var rr = document.getElementById("result");
				var ctx = rr.getContext("2d");

				// Render image up to scale (not an intuitive task)
				rr.width = DISPLAY_SIZE.width;
				rr.height = DISPLAY_SIZE.height;
				ctx.putImageData(imageData, 0, 0);
				ctx.scale(DISPLAY_SIZE.width/size.width, DISPLAY_SIZE.height/size.height);
				ctx.drawImage(rr, 0, 0);
				console.log("Render Complete.  Napp =", napp, ", Ncoh =", ncoh);
				document.getElementById("spinner").style.display = "none";
			});
		}
	};
	var loadImage = function(sel, src, im){
		Caman(sel, src, function(){
			this.resize(size);
			this.render();
			loadCb(this.originalVisiblePixels(), im);
		});
	};
	var loadCanvas = function(id, sel, im){
		var dd1 = document.getElementById(id);
		var dd2 = document.getElementById(sel);
		var ctx1 = dd1.getContext("2d");
		var ctx2 = dd2.getContext("2d");
		dd2.width = size.width;
		dd2.height = size.height;
		ctx2.scale(size.width/DISPLAY_SIZE.width, size.height/DISPLAY_SIZE.height);
		ctx2.drawImage(dd1, 0, 0);

		loadCb(ctx2.getImageData(0,0,size.width,size.height).data, im);
	};

	switch(scene){
		case "shore":
		loadImage("#A", "images/newflower-src.jpg", "A");
		loadImage("#Ap", "images/newflower-blur.jpg", "Ap");
		loadImage("#B", "images/toy-newshore-src.jpg", "B");
		loadImage("#Bp", "images/toy-newshore-src.jpg", "Bp");
		break;

		case "stl":
		loadImage("#A", "images/stl-mask.jpg", "A");
		loadImage("#Ap", "images/stl-aerial.jpg", "Ap");
		loadImage("#B", "images/stl-newmask.jpg", "B");
		loadCb(null, "Bp");
		break;

		case "stl-custom":
		loadImage("#A", "images/stl-mask.jpg", "A");
		loadImage("#Ap", "images/stl-aerial.jpg", "Ap");
		loadCanvas("draw", "B", "B");
		loadCb(null, "Bp");
		break;

		case "potomac":
		loadImage("#A", "images/potomac-mask.jpg", "A");
		loadImage("#Ap", "images/potomac.jpg", "Ap");
		loadImage("#B", "images/potomac-newmask.jpg", "B");
		loadCb(null, "Bp");
		break;

		case "oxbow":
		loadImage("#A", "images/oxbow-mask.jpg", "A");
		loadImage("#Ap", "images/oxbow.jpg", "Ap");
		loadImage("#B", "images/oxbow-newmask.jpg", "B");
		loadCb(null, "Bp");
		break;
	}

	document.getElementById("spinner").style.display = "block";
}

function render(){
	runAlgorithm("stl-custom", true, $("#coherenceSlider").attr("data-slider"), $("#resolutionSlider").attr("data-slider"), {width:320, height:240});
}
