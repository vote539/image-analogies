// Most of this code is borrowed from
// http://www.williammalone.com/articles/create-html5-canvas-javascript-drawing-app/

function DoDraw(){
	var dd = document.getElementById("draw");
	var context = dd.getContext("2d");
	var clickX = new Array();
	var clickY = new Array();
	var clickDrag = new Array();
	var paint;

	function addClick(x, y, dragging)
	{
	  clickX.push(x);
	  clickY.push(y);
	  clickDrag.push(dragging);
	}
	function redraw(){
    context.fillStyle = "#CCCCCC";
    context.fillRect(0,0,context.canvas.width,context.canvas.height);
	  
	  context.strokeStyle = "#333333";
	  context.lineJoin = "round";
	  context.lineWidth = 40;
				
	  for(var i=0; i < clickX.length; i++) {		
	    context.beginPath();
	    if(clickDrag[i] && i){
	      context.moveTo(clickX[i-1], clickY[i-1]);
	     }else{
	       context.moveTo(clickX[i]-1, clickY[i]);
	     }
	     context.lineTo(clickX[i], clickY[i]);
	     context.closePath();
	     context.stroke();
	  }
	}

	this.clear = function(){
		clickX = [];
		clickY = [];
		clickDrag = [];
	}

	dd.addEventListener("mousedown", function(e){
	  var mouseX = e.pageX - this.offsetLeft;
	  var mouseY = e.pageY - this.offsetTop;
			
	  paint = true;
	  addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
	  redraw();
	});
	dd.addEventListener("mousemove", function(e){
	  if(paint){
	    addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
	    redraw();
	  }
	});
	dd.addEventListener("mouseup", function(e){
	  paint = false;
	});
	dd.addEventListener("mouseleave", function(e){
	  paint = false;
	});

	redraw();
}

var drawer = new DoDraw();
