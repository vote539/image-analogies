importScripts("polyfill.js");

var RAND_RESOLUTION;
var COHERENCE_WEIGHT;

var napp, ncoh;
function createImageAnalogy(rA,rAp,rB,rBp,size,takeColor,cb){
	var A = new FeaturePyramid([size], [rA], takeColor);
	var Ap = new FeaturePyramid([size], [rAp], takeColor);
	var B = new FeaturePyramid([size], [rB], takeColor);
	var Bp = new FeaturePyramid([size], [rBp], takeColor);
	var s = [];
	var l = 0, p;
	var np = size.width*size.height;
	s[l] = new Array(np).fill(0);
	napp = 0;
	ncoh = 0;
	for(var q=0; q<np; q++){
		p = bestMatch(A,Ap,B,Bp,s,l,q);
		Bp.set(l,q,Ap.get(l,p));
		s[l][q] = p;
	}

	cb(Bp.toRGB(l), napp, ncoh);
}

function bestMatch(A,Ap,B,Bp,s,l,q){
	var papp = bestApproximateMatch(A,Ap,B,Bp,l,q);
	var pcoh = bestCoherenceMatch(A,Ap,B,Bp,s,l,q);
	var dapp = A.nbhood(l,papp).dist(B.nbhood(l,q));
	var dcoh = A.nbhood(l,pcoh).dist(B.nbhood(l,q));

	var L = 0;
	if(dcoh < dapp*(1+Math.pow(2,l-L)*COHERENCE_WEIGHT)){
		ncoh++;
		return pcoh;
	}else{
		napp++;
		return papp;
	}
}

function bestApproximateMatch(A,Ap,B,Bp,l,q){
	var fv1 = B.nbhood(l,q,"5x5");
	// var fv2 = Bp.nbhood(l,q,"prime");

	// Perform a randomized brute force nearest-neighbor search on A
	var size = A.dims(l);
	var np = size.width*size.height;
	var p;
	var bestd = Number.MAX_VALUE;
	for(var i=0; i<np; i+=(Math.random()*RAND_RESOLUTION)>>0){
		var d = A.nbhood(l,i,"5x5").dist(fv1);
		if(d<bestd){
			bestd = d;
			p = i;
		}
	}
	return p;
}

function bestCoherenceMatch(A,Ap,B,Bp,s,l,q){
	var bestdr = Bp.coherence(l,q,function(dr,fv2){
		// dr is the location relative to q.
		var fv1 = Ap.nbhood(l, s[l][q+dr]-dr, "prime");
		return fv1.dist(fv2);
	});
	return s[l][q+bestdr]-bestdr;
}

// A discrete Gaussian kernel
var Gauss = [
	0.002915, 0.013064, 0.021539, 0.013064, 0.002915,
	0.013064, 0.058550, 0.096532, 0.058550, 0.013064,
	0.021539, 0.096532, 0.159150, 0.096532, 0.021539,
	0.013064, 0.058550, 0.096532, 0.058550, 0.013064,
	0.002915, 0.013064, 0.021539, 0.013064, 0.002915 
];

function FeatureVector(){
	var features = new Array(25).fill(-1);

	// Public Methods
	this.set = function(xy, f){
		var i = xy[0]+2+(xy[1]+2)*5;
		features[i] = f;
	};
	this.at = function(loc){
		return features[loc];
	};
	this.dist = function(o){
		// Compute and return the squared L2-distance using a Gaussian kernel
		var dist = 0, allgauss = 0, a, b, d;
		for(var i=0; i<25; i++){
			a = features[i];
			b = o._all[i];
			if(a!==-1 && b!==-1){
				d = a-b;
				dist += d*d*Gauss[i];
				allgauss += Gauss[i];
			}
		}

		// Normalize (to prevent underweighted border pixels) and return
		if(allgauss===0){
			return Number.MAX_VALUE;
		}else{
			return dist / allgauss;
		}
	};
	this._all = features;
}

function FeaturePyramid(sizes, RGBAarr, takeColor){
	// Private Fields
	var features = [];
	var yiqPyramid = [];
	var nbhoodStore = [];

	// Private Methods
	var rgbaArrayToYIQ = function(RGBA){
		var N = RGBA.length;
		var YIQ = [];
		for(var i=0; i<N; i+=4){
			YIQ.push([
				0.299*RGBA[i] + 0.587*RGBA[i+1] + 0.114*RGBA[i+2],
				0.596*RGBA[i] - 0.275*RGBA[i+1] - 0.321*RGBA[i+2],
				0.212*RGBA[i] - 0.528*RGBA[i+1] + 0.311*RGBA[i+2]
			]);
		}
		return YIQ;
	};
	var yiqToRGBAArray = function(YIQ){
		var N = YIQ.length*4;
		var RGBA = new Array(N).fill(255);
		for(var i=0; i<N; i+=4){
			var yiq = YIQ[i/4];
			RGBA[i]   = yiq[0] + 0.956*yiq[1] + 0.621*yiq[2];
			RGBA[i+1] = yiq[0] - 0.272*yiq[1] - 0.647*yiq[2];
			RGBA[i+2] = yiq[0] - 1.108*yiq[1] + 1.705*yiq[2];
		}
		return RGBA;
	};
	var locToXY = function(l,p){
		return [
			(p % sizes[l].width),
			(p / sizes[l].width)>>0 // integer division
		];
	};
	var xyToLoc = function(l,xy){
		// Check bounds
		if(xy[0]>=0 && xy[1]>=0 && xy[0]<sizes[l].width && xy[1]<sizes[l].height){
			return xy[0] + sizes[l].width*xy[1];
		}else{
			return -1;
		}
	};
	var yiqToFeature = function(yiq){
		return yiq[0];
	};

	// Public Methods
	this.f = function(l,p){
		var res = features[l][p];
		if(typeof(res)==="undefined") return -1;
		return res;
	};
	this.dims = function(l){
		return sizes[l];
	};
	this.nbhood = function(l,p,type){
		var key = l+";"+p+";"+type;
		if(nbhoodStore[key]){
			return nbhoodStore[key];
		}

		var xy = locToXY(l,p);
		var d = (type==="3x3" ? 1 : 2);
		var fv = new FeatureVector(type);
		var newxy, loc;

		// Make sure p exists
		if(p<0 || p>=sizes[l].width*sizes[l].height || typeof(p)!=="number"){
			return fv; // all -1
		}

		// Read pixels in scan order
		for(var j=-d; j<=d; j++){
			for(var i=-d; i<=d; i++){
				newxy = [xy[0]+i, xy[1]+j];
				loc = xyToLoc(l,newxy);

				if(loc !== -1){
					fv.set([i,j], features[l][loc]);
				}

				// Terminate if type is "prime" and we're evaluating the center
				if(type==="prime" && loc===p){
					break;
				}
			}
		}

		nbhoodStore[key] = fv;
		return fv;
	};
	this.coherence = function(l,p,cb){
		var xy = locToXY(l,p);
		var bestd = Number.MAX_VALUE;
		var newxy, loc, dr, bestdr;

		// Read pixels in scan order
		for(var j=-1; j<=1; j++){
			for(var i=-1; i<=1; i++){
				dr = i+sizes[l].width*j;
				loc = p+dr;

				// Continue if this xy point doesn't exist.
				// Break if we've scanned our way to the center.
				if(loc<0 || loc>=sizes[l].width*sizes[l].height) continue;
				if(loc === p) break;

				// Call the CB to get the distance metric
				var d = cb(dr, this.nbhood(l,loc,"prime"));

				// Update the guess?
				if(d<bestd){
					bestd = d;
					bestdr = dr;
				}
			}
		}

		return bestdr;
	};
	this.toRGB = function(l){
		return yiqToRGBAArray(yiqPyramid[l]);
	};
	this.get = function(l,p){
		return yiqPyramid[l][p];
	}
	this.set = function(l,p,value){
		if(takeColor){
			yiqPyramid[l][p] = value;
		}else{
			yiqPyramid[l][p][0] = value[0];
		}
		features[l][p] = yiqToFeature(value);
	};
	this._all = features;

	// Constructor
	if(RGBAarr[0]){
		var YIQ;
		for(var l=0; l<sizes.length; l++){
			YIQ = rgbaArrayToYIQ(RGBAarr[l]);
			yiqPyramid.push(YIQ);
			features.push(YIQ.map(yiqToFeature));
		}
	}else{
		var np;
		for(var l=0; l<sizes.length; l++){
			np = sizes[l].width*sizes[l].height;
			features.push(new Array(np).fill(0));
			yiqPyramid.push(new Array(np).fill([0,0,0]));
		}
	}
}

// Worker Bindings
self.addEventListener("message", function(e){
	var RGBAs = e.data.RGBAs;
	var size = e.data.size;
	var takeColor = e.data.takeColor;

	COHERENCE_WEIGHT = e.data.coherence;
	RAND_RESOLUTION = e.data.resolution;

	createImageAnalogy(
		RGBAs["A"],
		RGBAs["Ap"],
		RGBAs["B"],
		RGBAs["Bp"],
		size,
		takeColor,
		function(result, napp, ncoh){
			self.postMessage({
				result: result,
				napp: napp,
				ncoh: ncoh
			});
		}
	);
});
